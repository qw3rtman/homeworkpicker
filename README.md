HomeworkPicker
==============

When you just can't decide what assignment to start on.

Usage
-----
Simply run
`javac HomeworkPicker.java`
to compile it and then `java HomeworkPicker` to run it. :)

Extending
---------
All code is in one class in `HomeworkPicker.java`. There's not much there, which just means more opportunities for customization! ;)
