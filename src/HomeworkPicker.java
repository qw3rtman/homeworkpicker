import java.lang.Math;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HomeworkPicker
{
	private int total;

	public HomeworkPicker(int total)
	{
		setTotal(total);
	}

	public void setTotal(int total)
	{
		this.total = total;
	}

	public int[] pick()
	{
		int[] homework = new int[this.total];
		for (int i = 0; i < homework.length; i++) {
			homework[i] = i + 1;
		}

		Random random = new Random();
		for (int i = homework.length - 1; i > 0; i--) {
			int index = random.nextInt(i + 1);
			// Simple swap
			int a = homework[index];
			homework[index] = homework[i];
			homework[i] = a;
		}

		return homework;
	}

	public String toString()
	{
		return Arrays.toString(pick());
	}

	public static void main(String args[])
	{
		Scanner keyboard = new Scanner(System.in);

		System.out.print("How many assignments? ");
		int assignments = keyboard.nextInt();

		HomeworkPicker test = new HomeworkPicker(assignments);
		System.out.println(test);
	}
}
